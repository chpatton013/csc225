; Christopher Patton
; 10/10/11
; Lab 3: Encryption
;
; R0 - I/O
; R1 - KEY
; R2 - MAX
; R3 - NDX
; R4 - ASCII
; R5 - TEMP
;
; ASCII 0:  48
; ASCII LB: 10
; ASCII CR: 13

      .ORIG X3000

; initialize registers
START AND      R0,R0,#0
      AND      R1,R1,#0
      LD       R2,ITR
      AND      R3,R3,#0
      AND      R5,R5,#0

; print key prompt
PKEY  LEA      R0,INS1
      PUTS                 ; PRINT KEY PROMPT

; get key, echo, store, subtract ASCZR, print \n
GKEY  LD       R4,ASCZR
      GETC                 ; GET KEY
      OUT                  ; ECHO KEY
      ADD      R1,R0,R4    ; CONVERT FROM ASCII
      LD       R0,ASCLB
      NOT      R0,R0
      ADD      R0,R0,#1
      OUT                  ; PRINT \n

; print input prompt
PINP  LEA      R0,INS2
      PUTS                 ; PRINT MESSAGE PROMPT

; get char, echo, encrypt, store
INP   LD       R4,ASCLB
      GETC                  ; GET CHAR
      OUT                   ; ECHO CHAR
      ADD      R5,R0,R4
      BRz      NULL         ; IF \n, END
      AND      R5,R5,#1     ; UNMASK LAST BIT
      BRz      #2           ; IF ZERO, ADD 1
      ADD      R0,R0,#-1    ; ELSE, SUBTRACT 1
      BRnzp    #1
      ADD      R0,R0,#1
      ADD      R0,R0,R1     ; ADD KEY
      LEA      R5,ENCS      ; STORE ENCR ADDR IN R5
      ADD      R5,R3,R5     ; INCREMENT TO CURR ADDR
      STR      R0,R5,#0     ; STORE CHAR AT R5 ADDR
      ADD      R3,R3,#1     ; NDX++
      ADD      R5,R3,#-1
      NOT      R5,R5
      ADD      R5,R2,R5     ; 20 - NDX
      BRp      INP          ; IF > 0, REPEAT

; store null
NULL  LEA      R5,ENCS
      ADD      R5,R5,R3
;      ADD      R5,R5,#1     ; STORE NULL ADDR IN R6
      AND      R0,R0,#0
      STR      R0,R5,#0     ; ADD NULL

; print output
OUTP  LEA      R0,OUTS
      PUTS
      LEA      R0,ENCS
      PUTS
      LD       R0,ASCLB
      NOT      R0,R0
      ADD      R0,R0,#1
      OUT

      HALT

ITR   .FILL    #20
ASCZR .FILL    #-48
ASCLB .FILL    #-10
ASCCR .FILL    #-13
INS1  .STRINGZ "Encryption Key (1-9): "
INS2  .STRINGZ "Input Message: "
OUTS  .STRINGZ "Encrypted Message: "
ENCS  .BLKW    21

      .END
