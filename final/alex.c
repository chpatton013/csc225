#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_ARGS 2
#define MAX_CHARS 100
#define MAX_LENGTH 4

int main(int argc, char *argv[]) {
   int numWords = 0, goodWords = 0;
   char *str = malloc(MAX_CHARS), *temp = malloc(MAX_CHARS), *orig = temp;
   FILE *fp;

   if (argc != NUM_ARGS) {
      printf("Usage: a.out <file>\n");
      return -1;
   }

   fp = fopen(argv[1], "r");
   if (fp == NULL) {
      printf("Unable to open the file.\n");
      return -2;
   }

   while ((str = fgets(str, MAX_CHARS, fp))) {
      temp = strncpy(orig, str, MAX_CHARS);

      temp = strtok(temp, " \n,.?");
      if (temp != NULL) {
         ++numWords;

         if (strlen(temp) <= MAX_LENGTH) {
            ++goodWords;
            printf("%s\n", temp);
         }

         while (temp = strtok(NULL, " \n,.?")) {
            ++numWords;

            if (strlen(temp) <= MAX_LENGTH) {
               ++goodWords;
               printf("%s\n", temp);
            }
         }
      }
   }

   if (numWords - goodWords < goodWords)
      printf("** Alex would like this story. **\n");
   else
      printf("** This story is too hard. **\n");

   return 0;
}
