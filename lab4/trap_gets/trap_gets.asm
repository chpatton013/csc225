; TRAP_GETS
; CHRISTOPHER PATTON
;
; R0: ADDR OF NEW STR
; R1: STATUS AND DATA REGISTERS
; R2: NEGATIVE ASCII LINE BREAK
; R3: TEMP MATH
; R4: CURR ADDR TO WRITE TO

           .ORIG  x3300
TRAP_GETS  ST     R1,REG1      ; SAVE REGISTERS
           ST     R2,REG2
           ST     R3,REG3
           ST     R4,REG4

           LD     R2,ASCII_LB  ; INITIALIZE REGISTERS
           ADD    R4,R0,#0

LOOP       LDI    R1,KBSR      ; CHECK IF READY
           BRzp   LOOP
           LDI    R1,KBDR      ; GET CHAR
           ADD    R3,R1,R2     ; CHECK FOR '\n'
           BRz    NULL
           STI    R1,DDR       ; ECHO
           STR    R1,R4,#0     ; STORE CHAR
           ADD    R4,R4,#1     ; INCREMENT
           BRnzp  LOOP

NULL       AND    R3,R3,#0
           STR    R3,R4,#0     ; APPEND NULL

           LD     R1,REG1      ; RELOAD REGISTERS
           LD     R2,REG2
           LD     R3,REG3
           LD     R4,REG4

           RET

REG1       .FILL  x0000
REG2       .FILL  x0000
REG3       .FILL  x0000
REG4       .FILL  x0000
KBSR       .FILL  xFE00
KBDR       .FILL  xFE02
DDR        .FILL  xFE06
ASCII_LB   .FILL  xFFF6

           .END