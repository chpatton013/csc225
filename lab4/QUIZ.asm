; LAB4 QUIZ
; CHRISTOPHER PATTON
;
; R0: STRING ADDR'S
; R1: QUESTION NUM
; R2: TOTAL SCORE

          .ORIG     x3000

          LDI       R0,INTRO       ; INITIALIZE REGISTERS
          AND       R1,R1,#0
          AND       R2,R2,#0

          PUTS                     ; PRINT WELCOME MESSAGE

          LDI       R0,Q1          ; PRINT QUESTION 1
          PUTS
          JSR       ANSWER         ; GET ANSWER AND SCORE
          ADD       R1,R1,#1

          LDI       R0,Q2          ; PRINT QUESTION 1
          PUTS
          JSR       ANSWER         ; GET ANSWER AND SCORE
          ADD       R1,R1,#1

          LDI       R0,Q3          ; PRINT QUESTION 1
          PUTS
          JSR       ANSWER         ; GET ANSWER AND SCORE

          JSR       RESULT         ; DISPLAY RESULTS

          HALT

INTRO     .FILL     x3300
Q1        .FILL     x3301
Q2        .FILL     x3302
Q3        .FILL     x3303


; ANSWER
;
; R0: INPUT
; R1: QUESTION NUM
; R2: TOTAL SCORE
; R3: ASCII
; R4: MATH TEMP

ANSWER    ST        R7,REG7
          ST        R0,REG0        ; REGISTER INITIALIZATION
          GETC
          LD        R3,ASCII

          OUT
          ADD       R0,R0,R3       ; CONVERT FROM ASCII

QUES      ADD       R3,R1,#-2      ; CALC QUESTION
          BRz       QUES3
          ADD       R3,R1,#-1
          BRz       QUES2

QUES1     LD        R3,Q1_A        ; SET ADDRESS OF FIRST
          BRnzp     SC
QUES2     LD        R3,Q2_A
          BRnzp     SC
QUES3     LD        R3,Q3_A

SC        ADD       R0,R0,R3       ; GET SCORE AT ADDR
          LDR       R0,R0,#0
          ADD       R2,R0,R2       ; INCR TOTAL SCORE

          LD        R0,REG0
          LD        R7,REG7
          RET

REG0      .FILL     #0
REG7      .FILL     #0
ASCII     .FILL     #-49
Q1_A      .FILL     x3308
Q2_A      .FILL     x330C
Q3_A      .FILL     x3310


; RESULT
;
; R0: STRING ADDR
; R1: MATH TEMP
; R2: TOTAL SCORE

RESULT    ST        R7,REG7
          LEA       R0,GRADE
          PUTS

          ADD       R1,R2,#-14
          ADD       R1,R1,#-7
          BRp       GR_A
          ADD       R1,R2,#-14
          BRp       GR_B
          ADD       R1,R2,#-7
          BRp       GR_C
          BRnzp     GR_F

GR_A      LDI       R0,RES1
          BRnzp     FIN
GR_B      LDI       R0,RES2
          BRnzp     FIN
GR_C      LDI       R0,RES3
          BRnzp     FIN
GR_F      LDI       R0,RES4

FIN       PUTS
          LD        R7,REG7
          RET

RES1      .FILL     x3304
RES2      .FILL     x3305
RES3      .FILL     x3306
RES4      .FILL     x3307
GRADE     .STRINGZ  "\n\nFinal Grade:\n"

          .END