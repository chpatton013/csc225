; RESULTS
;
; R0: STRING ADDR
; R1: MATH TEMP
; R2: TOTAL SCORE

          .ORIG     x3100

RESULTS   ST        R7,REG7
          LEA       R0,GRADE
          PUTS

          ADD       R1,R2,#-14
          ADD       R1,R1,#-7
          BRp       GR_A
          ADD       R1,R2,#-14
          BRp       GR_B
          ADD       R1,R2,#-7
          BRp       GR_C
          BRnzp     GR_F

GR_A      LDI       R0,RES1
          BRnzp     FIN
GR_B      LDI       R0,RES2
          BRnzp     FIN
GR_C      LDI       R0,RES3
          BRnzp     FIN
GR_F      LDI       R0,RES4

FIN       PUTS
          LD        R7,REG7
          RET

REG7      .FILL     #0
RES1      .FILL     x3304
RES2      .FILL     x3305
RES3      .FILL     x3306
RES4      .FILL     x3307
GRADE     .STRINGZ  "\n\nFinal Grade:\n"

          .END