; LAB4 DATA
; CHRISTOPHER PATTON

          .ORIG     x3300

START     .FILL     x3314          ; x3300
QUES1     .FILL     x33A2          ; x3301
QUES2     .FILL     x3415          ; x3302
QUES3     .FILL     x3470          ; x3303
RESULT1   .FILL     x34D5          ; x3304
RESULT2   .FILL     x34F5          ; x3305
RESULT3   .FILL     x351C          ; x3306
RESULT4   .FILL     x3542          ; x3307

Q1_R1     .FILL     #10            ; x3308
Q1_R2     .FILL     #0             ; x3309
Q1_R3     .FILL     #7             ; x330A
Q1_R4     .FILL     #3             ; x330B
Q2_R1     .FILL     #7             ; x330C
Q2_R2     .FILL     #10            ; x330D
Q2_R3     .FILL     #0             ; x330E
Q2_R4     .FILL     #3             ; x330F
Q3_R1     .FILL     #0             ; x3310
Q3_R2     .FILL     #3             ; x3311
Q3_R3     .FILL     #7             ; x3312
Q3_R4     .FILL     #10            ; x3313

INTRO     .STRINGZ  "Please take your seat and prepare for the final exam.\nThink carefully about each question, and remember,\nkeep your eyes on your own screen!\n\n"
Q1        .STRINGZ  "How often do you use your blinker?\n   1 - Always\n   2 - Never\n   3 - Sometimes\n   4 - When the police are present\n"
Q2        .STRINGZ  "\nWhich operating system do you prefer?\n   1 - Linux\n   2 - Unix\n   3 - Mac\n   4 - Windows\n"
Q3        .STRINGZ  "\nWhat is your favorite programming language?\n   1 - Java\n   2 - C\n   3 - LC3 Assmebly\n   4 - Binary\n"
RES1      .STRINGZ  "A+! Way to take the high road!\n"
RES2      .STRINGZ  "B: Good work! Not quite there though.\n"
RES3      .STRINGZ  "C: Eh... You could have done better.\n"
RES4      .STRINGZ  "FAILURE!!! Stop taking the easy way out!\n"

          .END