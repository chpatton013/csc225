; LAB4 QUIZ
; CHRISTOPHER PATTON
;
; R0: STRING ADDR'S
; R1: QUESTION NUM
; R2: TOTAL SCORE

          .ORIG     x3000

          LDI       R0,INTRO       ; INITIALIZE REGISTERS
          AND       R1,R1,#0
          AND       R2,R2,#0

          PUTS                     ; PRINT WELCOME MESSAGE

          LDI       R0,Q1          ; PRINT QUESTION 1
          PUTS
          LD        R0,ANSWER
          JSRR      R0             ; GET ANSWER AND SCORE
          ADD       R1,R1,#1

          LDI       R0,Q2          ; PRINT QUESTION 1
          PUTS
          LD        R0,ANSWER
          JSRR      R0             ; GET ANSWER AND SCORE
          ADD       R1,R1,#1

          LDI       R0,Q3          ; PRINT QUESTION 1
          PUTS
          LD        R0,ANSWER
          JSRR      R0             ; GET ANSWER AND SCORE

          LD        R0,RESULT
          JSRR      R0             ; DISPLAY RESULTS

          HALT

INTRO     .FILL     x3300
Q1        .FILL     x3301
Q2        .FILL     x3302
Q3        .FILL     x3303
ANSWER    .FILL     x3050
RESULT    .FILL     x3100

          .END