; ANSWER
;
; R0: INPUT
; R1: QUESTION NUM
; R2: TOTAL SCORE
; R3: ASCII
; R4: MATH TEMP

          .ORIG     x3050

ANSWER    ST        R7,REG7
          ST        R0,REG0        ; REGISTER INITIALIZATION
          GETC
          LD        R3,ASCII

          OUT
          ADD       R0,R0,R3       ; CONVERT FROM ASCII

QUES      ADD       R3,R1,#-2      ; CALC QUESTION
          BRz       QUES3
          ADD       R3,R1,#-1
          BRz       QUES2

QUES1     LD        R3,Q1_A        ; SET ADDRESS OF FIRST
          BRnzp     SC
QUES2     LD        R3,Q2_A
          BRnzp     SC
QUES3     LD        R3,Q3_A

SC        ADD       R0,R0,R3       ; GET SCORE AT ADDR
          LDR       R0,R0,#0
          ADD       R2,R0,R2       ; INCR TOTAL SCORE

          LD        R0,REG0
          LD        R7,REG7
          RET

REG0      .FILL     #0
REG7      .FILL     #0
ASCII     .FILL     #-49
Q1_A      .FILL     x3308
Q2_A      .FILL     x330C
Q3_A      .FILL     x3310

          .END