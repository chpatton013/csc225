/*
 * Christopher Patton
 * CSC225 Lab6
 * 11/2/11
 */

#include <stdio.h>

#define STACK_SIZE 10

int stackArr[STACK_SIZE];
int stackTop = 0;

int push(int value) {
   if (stackTop < STACK_SIZE) {
      stackArr[stackTop] = value;
      stackTop++;
      return 0;
   }
   else
      return 1;
}

int pop(int *value) {
   if (stackTop > 0) {
      stackTop--;
      *value = stackArr[stackTop];
      return 0;
   }
   else
      return 1;
}

int printStack(int mode) {
   int ndx;

   if (mode == 0)
      for (ndx = 0; ndx < stackTop; ndx++)
         printf("%d ", stackArr[ndx]);

   else if (mode == 1)
      for (ndx = 0; ndx < stackTop; ndx++)
         printf("%x ", stackArr[ndx]);

   else if (mode == 2)
      for (ndx = 0; ndx < stackTop; ndx++)
         printf("%c ", stackArr[ndx]);

   else
      return -1;

   printf("\n");

   return 0;
}

