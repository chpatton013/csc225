/*
 * Christopher Patton
 * CSC225 Lab6
 * 11/2/11
 */

#include <stdio.h>
#include "stack.h"

#define MODE_D 0
#define MODE_H 1
#define MODE_C 2

int main(void) {
   char cmd;
   int mode = 0;
   int err = 0;
   int val;

   printf("Welcome to the stack program.\n\n");

   while (1) {
      printf("Enter option: ");
      scanf(" %c", &cmd);

      if (cmd == 'x') {
         printf("Goodbye!\n");
         return 0;
      }

      else if (cmd == 'd')
         mode = MODE_D;
      else if (cmd == 'h')
         mode = MODE_H;
      else if (cmd == 'c')
         mode = MODE_C;

      else if (cmd == 'u') {
         printf("What number? ");
         scanf("%d", &val);

         err = push(val);
         if (err)
            printf("Overflow!!!\n");
      }
      else if (cmd == 'o') {
         err = pop(&val);
         if (err)
            printf("Underflow!!!\n");
         else
            printf("Popped %d\n", val);
      }

      printf("Stack: ");
      printStack(mode);

      printf("\n");
   }
}
