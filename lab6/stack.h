/*
 * Christopher Patton
 * CSC225 Lab6
 * 11/2/11
 */

int push(int value);
int pop(int *value);
int printStack(int mode);

