	.ORIG	x3000

	LD	R1,TVT
	LD	R2,T26
	STR	R2,R1,#0

	LD	R1,IVT
	LD	R2,ISR
	STR	R2,R1,#0

	LD	R6,STK

	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen
	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen
	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen
	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen
	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen

	HALT

TVT	.FILL	x0026
T26	.FILL	x3300
IVT	.FILL	x0180
ISR	.FILL	x3500
STK	.FILL	x2FFF

        .END