	.ORIG	x3400

	LD	R0,ASCII  ; LOAD ASCII '*' CHAR

	AND	R1,R1,#0  ; RESET R1 TO 0
	ADD	R1,R1,#1  ; INCREMENT R1
	BRzp	#-2       ; IF NOT NEG, REPEAT
	OUT               ; IF NEG, PRINT *
	BRnzp	#-5       ; REPEAT

	HALT

ASCII	.FILL	x002A

	.END