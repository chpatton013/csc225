typedef struct LineType Line;
struct LineType {
   char *str;
   char **words;
   int numWords;
};

Line *constructLine(char *str);
int lineContainsWord(Line *line, char *word);

typedef struct NodeType Node;
struct NodeType {
   int occurrences, lineNum, *wordNum;
   Line *line;
   Node *next;
};

Node *constructNode(Line *line, int lineNum, char *word);
void printNode(Node *node);

