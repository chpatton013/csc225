; CHRISTOPHER PATTON
; CSC225 LAB7

	.ORIG	x3000

; print prompts and get input
; prompt for string
	LEA	R0, PROMPT1
	PUTS
	LEA	R0, STRING
	LD	R1, GETS
	JSRR	R1
	LD	R0, ASCIINL
	OUT
; prompt for token
	LEA	R0, PROMPT2
	PUTS
	GETC
	OUT
	ST	R0, TOKEN
	LD	R0, ASCIINL
	OUT

; print operation
	LEA	R0, OPSTR1
	PUTS
	LD	R0, TOKEN
	OUT
	LEA	R0, OPSTR2
	PUTS
	LD	R0, ASCIINL
	OUT
	LEA	R0, STRING
	PUTS
	LD	R0, ASCIINL
	OUT

; call to recursive char count
	LD	R6, STACK
; push args
	LD	R0, TOKEN
	STR	R0, R6, #0
	ADD	R6, R6, #-1
	LEA	R0, STRING
	STR	R0, R6, #0
	ADD	R6, R6, #-1
; call
	JSR	FUNCT1

; return from function
; load rtn val
	LDR	R0, R6, #1
	ST	R0, COUNT
; pop rtn val and args
	ADD	R6, R6, #3

; print result
	LEA	R0, RESSTR1
	PUTS
	LD	R0, TOKEN
	OUT
	LEA	R0, RESSTR2
	PUTS
	LD	R0, COUNT
	LD	R1, ASCII0
	ADD	R0, R0, R1
	OUT
	LEA	R0, RESSTR3
	PUTS
	LD	R0, ASCIINL
	OUT

	HALT


; bookeeping
FUNCT1	AND	R0, R0, #0	; allocate rtn val
	STR	R0, R6, #0
	ADD	R6, R6, #-1
	STR	R7, R6, #0	; push rtn addr
	ADD	R6, R6, #-1

; logic
	LDR	R0, R6, #3	; curr str addr
	LDR	R1, R0, #0	; curr letter
	BRz	RTRN		; EOS, rtn 0

	LDR	R2, R6, #4	; token
	NOT	R2, R2		; negate
	ADD	R2, R2, #1
	ADD	R2, R1, R2
	BRnp	RECR		; !token, rtn recr
	ADD	R2, R2, #1	; token, rtn 1 + recr
	STR	R2, R6, #2

; recursion
; bookeeping
RECR	LDR	R1, R6, #4
	STR	R1, R6, #0	; push token
	ADD	R6, R6, #-1
	ADD	R0, R0, #1	; push string
	STR	R0, R6, #0
	ADD	R6, R6, #-1
; call
	JSR	FUNCT1

; return from function
; load rtn val
	LDR	R0, R6, #1
; pop rtn val and args
	ADD	R6, R6, #3

; compute and store rtn val
	LDR	R1, R6, #2
	ADD	R0, R0, R1
	STR	R0, R6, #2

; exiting
RTRN	LDR	R7, R6, #1	; pop rtn addr
	ADD	R6, R6, #1
	JMP	R7		; return to caller


COUNT	.FILL		#0
ASCIINL	.FILL		#10
ASCII0	.FILL		#48
GETS	.FILL		x3300
STACK	.FILL		x2FFF
PROMPT1	.STRINGZ	"Enter a string: "
PROMPT2	.STRINGZ	"Enter search token: "
OPSTR1	.STRINGZ	"Searching string for character '"
OPSTR2	.STRINGZ	"':"
RESSTR1	.STRINGZ	"Character '"
RESSTR2	.STRINGZ	"' found "
RESSTR3	.STRINGZ	" times."
TOKEN	.FILL		#0
STRING	.BLKW		#50

	.END