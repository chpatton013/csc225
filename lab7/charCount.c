/* Christopher Patton
 * CSC225 Lab7
 * 11/4/11
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_SIZE 1001
#define WORD_SIZE 101

int charCount(char * str, char * token);

int main(void) {
   char *token = calloc(sizeof(char), 1),
    *str = calloc(sizeof(char), STR_SIZE),
    *temp = calloc(sizeof(char), WORD_SIZE);
   int err, mode, count;
   FILE *file;

   printf("Entry mode (0 = user input, 1 = file input): ");
   scanf("%d", &mode);

   if (mode) {
      file = fopen("file.in", "r");
      while(fscanf(file, "%s", temp) != EOF)
         strcat(str, strcat(temp, " "));
   }
   else {
      printf("Enter a string: ");
      while (scanf("%s", temp) != EOF)
         strcat(str, strcat(temp, " "));
   }

   printf("Enter search token: ");
   scanf(" %c", token);

   printf("Searching string for character '%c':\n%s\n", *token, str);

   count = charCount(str, token);

   printf("Character '%c' found %d times.\n", *token, count);

   return 0;
}

int charCount(char * str, char * token) {
   if (*str == 0)
      return 0;
   else if (*str == *token)
      return 1 + charCount(str + 1, token);
   else
      return charCount(str + 1, token);
}
